from django.urls import path, include

from app import views

urlpatterns = [
    path('api/', include('app.api.urls')),
    path('', views.Login.as_view(), name='login'),
    path('base', views.Base.as_view(), name='base'),
    path('org/list', views.OrganizationList.as_view(), name='org_list'),
    path('doc/list', views.DocumentList.as_view(), name='doc_list'),
    path('doc/detail/<int:id>', views.DocumentDetail.as_view(), name='detail'),
    path('pro/list', views.ProductList.as_view(), name='pro_list'),
]
