from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from app.models.document import Document


class Product(models.Model):
    document = models.ForeignKey(Document, on_delete=models.SET_NULL, null=True)
    name = models.TextField(null=True, blank=True)
    measurement = models.TextField(null=True, blank=True)
    quantity = models.IntegerField(null=True, blank=True)
    sum = models.FloatField(null=True, blank=True)
    code = models.TextField(null=True, blank=True)


# @receiver(post_save, sender=Product)
# def create_code(sender, instance, created, **kwargs):
#     if created:
#         instance.code = generate